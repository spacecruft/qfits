# qfits
Spacecruft copy in `git` format of the upstream FTP repo. 

This git repo replays the history of qfits in tags. So the first
commit is from `qfits-4.0.2` the last commit is `qfits-6.2.0`.

If you want a particular version, just check it out, ala:

```
git checkout qfits-5.2.0
```

# Build
All versions build with the standard:

```
cd qfits/
./configure
make
sudo make install
```

# FTP
If you haven't connected to ftp or gopher in awhile, here's
some nostalgia:

```
$ ncftp ftp://ftp.eso.org/pub/qfits
NcFTP 3.2.5 (Feb 02, 2011) by Mike Gleason (http://www.NcFTP.com/contact/).
Connecting to 134.171.42.54...                                                                                                                                                        
::ffff:134.171.42.54 FTP server ready
Logging in...                                                                                                                                                                         
Welcome to the ftp server for ESO at Garching bei Muenchen
Guest access granted for anonymous.
Logged in to ftp.eso.org.                                                                                                                                                             
Current remote directory is /pub/qfits.
ncftp /pub/qfits > ls -l
-rw-r--r--   3197     3197            0   Apr 18  2002   _LATEST_VERSION_IS_6_2_0_
-rw-r--r--   3197     3197       158185   Jan 24  2002   qfits-4.0.2.tar.gz
-rw-r--r--   3197     3197       311545   Mar 11  2002   qfits-4.1.0.tar.gz
-rw-r--r--   3197     3197       328744   Apr 18  2002   qfits-4.2.0.tar.gz
-rw-r--r--   3197     3197       417158   Sep  6  2002   qfits-4.2.2.tar.gz
-rw-rw-r--   3197     3197       177503   May 22  2003   qfits-4.3.0.tar.gz
-rw-r--r--   3197     3197       178061   Jun 13  2003   qfits-4.3.1.tar.gz
-rw-r--r--   3197     3197       246105   Jul 10  2003   qfits-4.3.2.tar.gz
-rw-r--r--   3197     3197       247107   Sep 30  2003   qfits-4.3.3.tar.gz
-rw-r--r--   3197     3197       247098   Nov  5  2003   qfits-4.3.4.tar.gz
-rw-r--r--   3197     3197       252289   Nov 24  2003   qfits-4.3.5.tar.gz
-rw-r--r--   3197     3197       253160   Feb 25  2004   qfits-4.3.6.tar.gz
-rw-r--r--   3197     3197       253737   Mar  8  2004   qfits-4.3.7.tar.gz
-rw-r--r--   3197     3197       262658   Jun 23  2004   qfits-5.0.0.tar.gz
-rw-r--r--   3197     3197       299550   Feb 17  2005   qfits-5.1.0.tar.gz
-rw-r--r--   3197     3197       299541   Feb 23  2005   qfits-5.1.1.tar.gz
-rw-r--r--   3197     3197       300747   Mar 11  2005   qfits-5.2.0.tar.gz
-rw-r--r--   3197     3197       300215   Jun 30  2005   qfits-5.3.0.tar.gz
-rw-r--r--   3197     3197       300428   Jul 25  2005   qfits-5.3.1.tar.gz
-rw-r--r--   3197     3197       303131   Jul 21  2005   qfits-5.4.0.tar.gz
-rw-r--r--   3197     3197       464401   Jul 13  2006   qfits-6.0.0.tar.gz
-rw-r--r--   3197     3197       474766   Sep 21  2006   qfits-6.1.0.tar.gz
-rw-r--r--   3197     3197       479559   Mar  5  2007   qfits-6.2.0.tar.gz
-rw-r--r--   3197     3197           75   Jan 22  2007   README
ncftp /pub/qfits > cat README
This is the qfits ftp distribution directory
Last updated:
Mon Jan 22 2007
```

# License
The upstream early versions have an MIT-like permissive license.
At version 6.0.0, it appears to have switched to GPLv2.

